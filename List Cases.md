# List Cases

## Parking Lot

- case1 - **Given** a parking lot and a car **When** park the car , **Then** return a parking ticket.
- case2 - **Given** two parking lots and two cars **When** park two cars,Then **return** two parking lot.
- case3 - **Given** a ticket **When** fetch the car,**Then** return a right car.
- case4 - **Given** a car **When** park the car with no parking lot capacity,**Then** throw exception.
- case5 - **Given** a error ticket **When** fetch the car,**Then** throw exception.
- case6 - **Given** a used ticket **When** fetch the car,**Then** throw exception.

Standard Parking Boy

- case1 - **Given** a parking lot and a car **When** park the car , **Then** return a parking ticket.

- case2 - **Given** two parking lots and two cars **When** park two cars,Then **return** two parking lot.

- case3 - **Given** a ticket **When** fetch the car,**Then** return a right car.

- case4 - **Given** a car **When** park the car with no parking lot capacity,**Then** throw exception.

- case5 - **Given** a error ticket **When** fetch the car,**Then** throw exception.

- case6 - **Given** a used ticket **When** fetch the car,**Then** throw exception.

  

- case7 - **Given** two parking lots ,a standard parking boy and some cars,**when** first parking lot is not full,**then** park in this parking lot.

- case8 - **Given** two parking lots ,a standard  parking boy and some cars,**when** first parking lot is full,**then** park the next one.

- case9 - **Given** two parking lots ,a standard  parking boy and some cars,**when** both parking lots are full,**then** throw exception.

- case10 - **Given** a ticket **When** fetch the car,**Then** return a right car.

- case11 - **Given** a error ticket **When** fetch the car,**Then** throw exception.

- case12 - **Given** a used ticket **When** fetch the car,**Then** throw exception.



## Smart Parking Boy

- case1 - **Given** two parking lots ,a parking boy and some cars,**when** parking lots is not full,**then** park the more available one.
- case2 - **Given** two parking lot ,a parking boy and some cars,**when** both parking lots are full,**then** throw exception.
- case3 - **Given** a ticket **When** fetch the car,**Then** return a right car.
- case4 - **Given** a error ticket **When** fetch the car,**Then** throw exception.
- case5 - **Given** a used ticket **When** fetch the car,**Then** throw exception.



## Super Parking Boy

- case1 - **Given** two parking lots ,a parking boy and some cars,**when** parking lots is not full,**then** park the high available rate one.

- case2 - **Given** two parking lot ,a parking boy and some cars,**when** both parking lots are full,**then** throw exception.
- case3 - **Given** a ticket **When** fetch the car,**Then** return a right car.
- case4 - **Given** a error ticket **When** fetch the car,**Then** throw exception.
- case5 - **Given** a used ticket **When** fetch the car,**Then** throw exception.