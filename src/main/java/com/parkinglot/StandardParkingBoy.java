package com.parkinglot;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class StandardParkingBoy implements ParkingBoy {
    private List<ParkingLot> parkingLots = new ArrayList<>();

    public StandardParkingBoy(List<ParkingLot> parkingLots) {
        this.parkingLots = parkingLots;
    }

    public ParkingLot findFirstAvailableParkingLot(List<ParkingLot> parkingLots) {
        List<ParkingLot> availableParkingLots = parkingLots.stream().filter(parkingLot -> !parkingLot.isFull()).collect(Collectors.toList());
        if (availableParkingLots.size() == 0) {
            return null;
        } else {
            return availableParkingLots.get(0);
        }

    }

    public Ticket park(Car car) {
        ParkingLot parkingLot = findFirstAvailableParkingLot(parkingLots);
        if (parkingLot == null) {
            throw new ParkingException("No available position.");
        }
        return parkingLot.park(car);
    }

    public Car fetch(Ticket ticket) {
        ParkingLot inParkingLot = null;
        for (ParkingLot parkingLot : parkingLots) {
            if (parkingLot.isValidTicket(ticket)) {
                inParkingLot = parkingLot;
            }
        }
        if (inParkingLot == null) {
            throw new ParkingException("Unrecognized parking ticket.");
        }
        return inParkingLot.fetch(ticket);
    }

}
