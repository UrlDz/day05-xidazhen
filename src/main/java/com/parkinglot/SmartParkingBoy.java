package com.parkinglot;

import java.util.*;

public class SmartParkingBoy implements ParkingBoy {

    private List<ParkingLot> parkingLots;
    private Map<ParkingLot, Integer> parkingLotAvailablePositonsMap;


    public SmartParkingBoy(List<ParkingLot> parkingLots) {
        this.parkingLots = parkingLots;
        this.parkingLotAvailablePositonsMap = new HashMap<>();
    }


    public void countParkingLotAvailablePositonsToMap(List<ParkingLot> parkingLots) {
        parkingLots
                .stream()
                .filter(parkingLot -> !parkingLot.isFull())
                .forEach((parkingLot) -> this.parkingLotAvailablePositonsMap.put(parkingLot, parkingLot.getavailablePositionNumer()));
    }

    public Ticket park(Car car) {
        this.countParkingLotAvailablePositonsToMap(this.parkingLots);
        if (this.parkingLotAvailablePositonsMap.isEmpty()) {
            throw new ParkingException("No available position.");
        }
        Optional<Map.Entry<ParkingLot, Integer>> moreAvailablePositionsParkingLot =
                this.parkingLotAvailablePositonsMap.entrySet()
                        .stream()
                        .max(Map.Entry.comparingByValue());
        this.parkingLotAvailablePositonsMap.clear();
        return moreAvailablePositionsParkingLot.get().getKey().park(car);
    }

    public Car fetch(Ticket ticket) {
        ParkingLot inParkingLot = null;
        for (ParkingLot parkingLot : parkingLots) {
            if (parkingLot.isValidTicket(ticket)) {
                inParkingLot = parkingLot;
            }
        }
        if (inParkingLot == null) {
            throw new ParkingException("Unrecognized parking ticket.");
        }
        return inParkingLot.fetch(ticket);
    }

}
