package com.parkinglot;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class ParkingServiceManager {
    private List<ParkingLot> parkingLots = new ArrayList<>();
    private ParkingBoy parkingBoy;

    public ParkingServiceManager(ParkingBoy parkingBoy) {
        this.parkingBoy = parkingBoy;
    }

    public ParkingServiceManager(List<ParkingLot> parkingLots) {
        this.parkingLots = parkingLots;
    }

    public ParkingLot findFirstAvailableParkingLot(List<ParkingLot> parkingLots) {
        List<ParkingLot> availableParkingLots = parkingLots.stream().filter(parkingLot -> !parkingLot.isFull()).collect(Collectors.toList());
        if (availableParkingLots.size() == 0) {
            return null;
        } else {
            return availableParkingLots.get(0);
        }

    }

    public Ticket park(Car car) {
        if (parkingBoy == null) {
            ParkingLot parkingLot = findFirstAvailableParkingLot(parkingLots);
            if (parkingLot == null) {
                throw new ParkingException("No available position.");
            }
            return parkingLot.park(car);
        } else {
            Ticket ticket = parkingBoy.park(car);
            if (ticket == null) {
                throw new ParkingException("No available position.");
            } else {
                return ticket;
            }
        }
    }

    public Car fetch(Ticket ticket) {
        ParkingLot inParkingLot = null;
        for (ParkingLot parkingLot : parkingLots) {
            if (parkingLot.isValidTicket(ticket)) {
                inParkingLot = parkingLot;
                break;
            }
            System.out.println(inParkingLot.toString());
        }

        if (parkingBoy == null) {
            return inParkingLot.fetch(ticket);
        } else {
            Car car = parkingBoy.fetch(ticket);
            if (car == null) {
                throw new ParkingException("Unrecognized parking ticket.");
            } else {
                return car;
            }
        }
    }

}
