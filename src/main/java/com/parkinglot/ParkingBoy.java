package com.parkinglot;

public interface ParkingBoy {
    Ticket park(Car car);

    Car fetch(Ticket ticket);
}
