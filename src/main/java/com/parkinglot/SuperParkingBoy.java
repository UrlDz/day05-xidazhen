package com.parkinglot;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public class SuperParkingBoy implements ParkingBoy {
    private List<ParkingLot> parkingLots;
    private Map<ParkingLot, Double> parkingLotAvailableRateMap;

    public SuperParkingBoy(List<ParkingLot> parkingLots) {
        this.parkingLots = parkingLots;
        this.parkingLotAvailableRateMap = new HashMap<>();
    }

    public void countParkingLotAvailableRateMapToMap(List<ParkingLot> parkingLots) {
        parkingLots
                .stream()
                .filter(parkingLot -> !parkingLot.isFull())
                .forEach((parkingLot) -> this.parkingLotAvailableRateMap.put(parkingLot, parkingLot.getAvailableRate()));
    }

    public Ticket park(Car car) {
        this.countParkingLotAvailableRateMapToMap(this.parkingLots);
        if (this.parkingLotAvailableRateMap.isEmpty()) {
            throw new ParkingException("No available position.");
        }
        Optional<Map.Entry<ParkingLot, Double>> moreAvailablePositionsParkingLot =
                this.parkingLotAvailableRateMap.entrySet()
                        .stream()
                        .max(Map.Entry.comparingByValue());
        this.parkingLotAvailableRateMap.clear();
        return moreAvailablePositionsParkingLot.get().getKey().park(car);
    }

    public Car fetch(Ticket ticket) {
        ParkingLot inParkingLot = null;
        for (ParkingLot parkingLot : parkingLots) {
            if (parkingLot.isValidTicket(ticket)) {
                inParkingLot = parkingLot;
            }
        }
        if (inParkingLot == null) {
            throw new ParkingException("Unrecognized parking ticket.");
        }
        return inParkingLot.fetch(ticket);
    }

}
