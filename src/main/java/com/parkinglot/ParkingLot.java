package com.parkinglot;

import java.awt.*;
import java.util.HashMap;
import java.util.Map;

public class ParkingLot {
    Map<Ticket, Car> parkingList = new HashMap<>();
    private Ticket ticket;
    private int capacity;

    public ParkingLot(int capacity) {
        this.capacity = capacity;
    }

    public boolean isFull() {
        return parkingList.size() >= capacity;
    }

    public boolean isValidTicket(Ticket ticket) {
        Car car = parkingList.get(ticket);
        return car != null;
    }

    public Ticket park(Car car) {
        if (isFull()) {
            throw new ParkingException("No available position.");
        }
        ticket = new Ticket(this);
        parkingList.put(ticket, car);
        return ticket;
    }

    public Car fetch(Ticket ticket) {
        if (!isValidTicket(ticket)) {
            throw new ParkingException("Unrecognized parking ticket.");
        }
        Car car = parkingList.get(ticket);
        parkingList.remove(ticket);
        return car;
    }

    public int getavailablePositionNumer() {
        return capacity - parkingList.size();
    }

    public double getAvailableRate() {
        return (double) (capacity - parkingList.size()) / capacity;
    }
}
