package com.parkinglot;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class ParkingServiceManagerTest {
    @Test
    void should_return_a_parking_ticket_when_given_a_car_own() {
        Car car = new Car();
        ParkingLot parkingLot = new ParkingLot(10);
        List<ParkingLot> parkingLots = new ArrayList<ParkingLot>();
        parkingLots.add(parkingLot);
        ParkingServiceManager parkingServiceManager = new ParkingServiceManager(parkingLots);
        Ticket ticket = parkingServiceManager.park(car);
        Assertions.assertNotNull(ticket);
    }

    @Test
    void should_return_a_parking_ticket_when_given_a_car_and_use_smart_parking_boy() {
        Car car = new Car();
        ParkingLot parkingLot = new ParkingLot(10);
        List<ParkingLot> parkingLots = new ArrayList<ParkingLot>();
        parkingLots.add(parkingLot);
        ParkingServiceManager parkingServiceManager = new ParkingServiceManager(new SmartParkingBoy(parkingLots));
        Ticket ticket = parkingServiceManager.park(car);
        Assertions.assertNotNull(ticket);
    }

    @Test
    void should_throw_exception_when_given_a_car_but_no_parking_position_own() {
        ParkingLot parkingLot = new ParkingLot(1);
        List<ParkingLot> parkingLots = new ArrayList<>();
        parkingLots.add(parkingLot);
        ParkingServiceManager parkingServiceManager = new ParkingServiceManager(new SmartParkingBoy(parkingLots));
        Car car1 = new Car();
        parkingServiceManager.park(car1);
        Car car2 = new Car();
        ParkingException parkingException = Assertions.assertThrows(ParkingException.class, () -> parkingServiceManager.park(car2));
        Assertions.assertEquals("No available position.", parkingException.getMessage());
    }

    @Test
    void should_throw_exception_when_given_a_car_but_no_parking_position_use_smart_parking_boy() {
        ParkingLot parkingLot = new ParkingLot(1);
        List<ParkingLot> parkingLots = new ArrayList<>();
        parkingLots.add(parkingLot);
        ParkingServiceManager parkingServiceManager = new ParkingServiceManager(parkingLots);
        Car car1 = new Car();
        parkingServiceManager.park(car1);
        Car car2 = new Car();
        ParkingException parkingException = Assertions.assertThrows(ParkingException.class, () -> parkingServiceManager.park(car2));
        Assertions.assertEquals("No available position.", parkingException.getMessage());
    }

    @Test
    void should_return_a_corresponding_car_when_given_a_valid_parking_ticket_and_use_smart_parking_boy() {
        ParkingLot parkingLot = new ParkingLot(10);
        List<ParkingLot> parkingLots = new ArrayList<>();
        parkingLots.add(parkingLot);
        ParkingServiceManager parkingServiceManager = new ParkingServiceManager(new SmartParkingBoy(parkingLots));
        Car car1 = new Car();
        Ticket ticket = parkingServiceManager.park(car1);
        Car car2 = parkingServiceManager.fetch(ticket);
        Assertions.assertEquals(car1, car2);
    }

    @Test
    void should_throw_exception_when_given_a_error_parking_ticket() {
        ParkingLot parkingLot = new ParkingLot(10);
        List<ParkingLot> parkingLots = new ArrayList<>();
        parkingLots.add(parkingLot);
        ParkingServiceManager parkingServiceManager = new ParkingServiceManager(new SmartParkingBoy(parkingLots));
        Ticket ticket = new Ticket();
        ParkingException parkingException = Assertions.assertThrows(ParkingException.class, () -> parkingServiceManager.fetch(ticket));
        Assertions.assertEquals("Unrecognized parking ticket.", parkingException.getMessage());
    }

    @Test
    void should_throw_exception_when_given_a_used_parking_ticket() {
        ParkingLot parkingLot = new ParkingLot(10);
        List<ParkingLot> parkingLots = new ArrayList<>();
        parkingLots.add(parkingLot);
        ParkingServiceManager parkingServiceManager = new ParkingServiceManager(new StandardParkingBoy(parkingLots));
        Car car1 = new Car();
        Ticket ticket = parkingServiceManager.park(car1);
        Car car2 = parkingServiceManager.fetch(ticket);
        Assertions.assertEquals(car1, car2);
        ParkingException parkingException = Assertions.assertThrows(ParkingException.class, () -> parkingServiceManager.fetch(ticket));
        Assertions.assertEquals("Unrecognized parking ticket.", parkingException.getMessage());
    }
}
