package com.parkinglot;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class SmartParkingBoyTest {
    @Test
    void should_return_a_parking_ticket_when_given_a_car_and_a_available_parking_lot() {
        Car car = new Car();
        ParkingLot parkingLot = new ParkingLot(10);
        List<ParkingLot> parkingLots = new ArrayList<ParkingLot>();
        parkingLots.add(parkingLot);
        SmartParkingBoy smartParkingBoy = new SmartParkingBoy(parkingLots);
        Ticket ticket = smartParkingBoy.park(car);
        Assertions.assertNotNull(ticket);
    }

    @Test
    void should_return_a_more_available_positions_parking_lot_ticket_when_given_a_car_and_two_parking_lots() {
        Car car = new Car();
        ParkingLot parkingLot1 = new ParkingLot(2);
        ParkingLot parkingLot2 = new ParkingLot(10);
        List<ParkingLot> parkingLots = new ArrayList<>();
        parkingLots.add(parkingLot1);
        parkingLots.add(parkingLot2);
        SmartParkingBoy smartParkingBoy = new SmartParkingBoy(parkingLots);
        Ticket ticket = smartParkingBoy.park(car);
        Assertions.assertNotNull(ticket);
        Assertions.assertEquals(parkingLot2, ticket.getParkingLot());
    }

    @Test
    void should_throw_exception_when_given_a_car_but_both_parking_lots_without_position() {
        ParkingLot parkingLot1 = new ParkingLot(1);
        ParkingLot parkingLot2 = new ParkingLot(1);
        List<ParkingLot> parkingLots = new ArrayList<>();
        parkingLots.add(parkingLot1);
        parkingLots.add(parkingLot2);
        SmartParkingBoy smartParkingBoy = new SmartParkingBoy(parkingLots);
        Car car1 = new Car();
        Car car2 = new Car();
        smartParkingBoy.park(car1);
        smartParkingBoy.park(car2);
        Car car3 = new Car();
        ParkingException parkingException = Assertions.assertThrows(ParkingException.class, () -> smartParkingBoy.park(car3));
        Assertions.assertEquals("No available position.", parkingException.getMessage());
    }

    @Test
    void should_return_two_different_available_positions_parking_tickets_when_given_two_cars_and_two_different_positions_parking_lots() {
        Car car1 = new Car();
        Car car2 = new Car();
        ParkingLot parkingLot1 = new ParkingLot(4);
        ParkingLot parkingLot2 = new ParkingLot(4);
        List<ParkingLot> parkingLots = new ArrayList<ParkingLot>();
        parkingLots.add(parkingLot1);
        parkingLots.add(parkingLot2);
        SmartParkingBoy smartParkingBoy = new SmartParkingBoy(parkingLots);
        Ticket ticket1 = smartParkingBoy.park(car1);
        Ticket ticket2 = smartParkingBoy.park(car2);
        Assertions.assertNotEquals(ticket1.getParkingLot(), ticket2.getParkingLot());
    }

    @Test
    void should_return_a_corresponding_car_when_given_a_valid_parking_ticket_and_a_parking_boy() {
        ParkingLot parkingLot = new ParkingLot(10);
        List<ParkingLot> parkingLots = new ArrayList<ParkingLot>();
        parkingLots.add(parkingLot);
        SmartParkingBoy smartParkingBoy = new SmartParkingBoy(parkingLots);
        Car car1 = new Car();
        Ticket ticket = smartParkingBoy.park(car1);
        Car car2 = smartParkingBoy.fetch(ticket);
        Assertions.assertEquals(car1, car2);
    }

    @Test
    void should_throw_exception_when_given_a_error_parking_ticket() {
        ParkingLot parkingLot = new ParkingLot(10);
        List<ParkingLot> parkingLots = new ArrayList<>();
        parkingLots.add(parkingLot);
        SmartParkingBoy smartParkingBoy = new SmartParkingBoy(parkingLots);
        ParkingException parkingException = Assertions.assertThrows(ParkingException.class, () -> smartParkingBoy.fetch(new Ticket()));
        Assertions.assertEquals("Unrecognized parking ticket.", parkingException.getMessage());
    }

    @Test
    void should_throw_exception_when_given_a_used_parking_ticket() {
        ParkingLot parkingLot = new ParkingLot(10);
        List<ParkingLot> parkingLots = new ArrayList<>();
        parkingLots.add(parkingLot);
        SmartParkingBoy smartParkingBoy = new SmartParkingBoy(parkingLots);
        Car car1 = new Car();
        Ticket ticket = smartParkingBoy.park(car1);
        Car car2 = smartParkingBoy.fetch(ticket);
        Assertions.assertEquals(car1, car2);
        ParkingException parkingException = Assertions.assertThrows(ParkingException.class, () -> smartParkingBoy.fetch(ticket));
        Assertions.assertEquals("Unrecognized parking ticket.", parkingException.getMessage());
    }
}
