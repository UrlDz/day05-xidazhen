package com.parkinglot;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class PakingLotTest {
    @Test
    void should_return_a_parking_ticket_when_given_a_car_and_a_parking_position() {
        ParkingLot parkingLot = new ParkingLot(10);
        Car car = new Car();
        Ticket ticket = parkingLot.park(car);
        Assertions.assertNotNull(ticket);
    }

    @Test
    void should_return_two_different_parking_tickets_when_given_two_cars_and_two_parking_positions() {
        ParkingLot parkingLot = new ParkingLot(10);
        Car car1 = new Car();
        Car car2 = new Car();
        Ticket ticket1 = parkingLot.park(car1);
        Ticket ticket2 = parkingLot.park(car2);
        Assertions.assertAll(
                () -> {
                    Assertions.assertNotNull(ticket1);
                },
                () -> {
                    Assertions.assertNotNull(ticket2);
                },
                () -> {
                    Assertions.assertNotEquals(ticket1, ticket2);
                }
        );
    }

    @Test
    void should_return_a_corresponding_car_when_given_a_valid_parking_ticket() {
        ParkingLot parkingLot = new ParkingLot(10);
        Car car1 = new Car();
        Ticket ticket = parkingLot.park(car1);
        Car car2 = parkingLot.fetch(ticket);
        Assertions.assertEquals(car1, car2);
    }

    @Test
    void should_throw_exception_when_given_a_car_but_no_parking_position() {
        ParkingLot parkingLot = new ParkingLot(10);
        for (int i = 0; i < 10; i++) {
            Car car = new Car();
            parkingLot.park(car);
        }
        Car car = new Car();
        ParkingException parkingException = Assertions.assertThrows(ParkingException.class, () -> parkingLot.park(car));
        Assertions.assertEquals("No available position.", parkingException.getMessage());
    }


    @Test
    void should_throw_exception_when_given_a_error_parking_ticket() {
        ParkingLot parkingLot = new ParkingLot(10);
        Ticket ticket = new Ticket();
        ParkingException parkingException = Assertions.assertThrows(ParkingException.class, () -> parkingLot.fetch(ticket));
        Assertions.assertEquals("Unrecognized parking ticket.", parkingException.getMessage());
    }

    @Test
    void should_throw_exception_when_given_a_used_parking_ticket() {
        ParkingLot parkingLot = new ParkingLot(10);
        Car car1 = new Car();
        Ticket ticket = parkingLot.park(car1);
        Car car2 = parkingLot.fetch(ticket);
        Assertions.assertEquals(car1, car2);
        ParkingException parkingException = Assertions.assertThrows(ParkingException.class, () -> parkingLot.fetch(ticket));
        Assertions.assertEquals("Unrecognized parking ticket.", parkingException.getMessage());
    }

}
