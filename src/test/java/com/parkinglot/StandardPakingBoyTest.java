package com.parkinglot;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class StandardPakingBoyTest {
    @Test
    void should_return_a_parking_ticket_when_given_a_car_and_a_parking_boy() {
        Car car = new Car();
        ParkingLot parkingLot = new ParkingLot(10);
        List<ParkingLot> parkingLots = new ArrayList<ParkingLot>();
        parkingLots.add(parkingLot);
        StandardParkingBoy standardParkingBoy = new StandardParkingBoy(parkingLots);
        Ticket ticket = standardParkingBoy.park(car);
        Assertions.assertNotNull(ticket);
    }

    @Test
    void should_return_two_different_parking_tickets_when_given_two_cars_and_a_parking_boy() {
        Car car1 = new Car();
        Car car2 = new Car();
        ParkingLot parkingLot = new ParkingLot(10);
        List<ParkingLot> parkingLots = new ArrayList<ParkingLot>();
        parkingLots.add(parkingLot);
        StandardParkingBoy standardParkingBoy = new StandardParkingBoy(parkingLots);
        Ticket ticket1 = standardParkingBoy.park(car1);
        Ticket ticket2 = standardParkingBoy.park(car2);
        Assertions.assertAll(
                () -> {
                    Assertions.assertNotNull(ticket1);
                },
                () -> {
                    Assertions.assertNotNull(ticket2);
                },
                () -> {
                    Assertions.assertNotEquals(ticket1, ticket2);
                }
        );
    }

    @Test
    void should_return_a_corresponding_car_when_given_a_valid_parking_ticket_and_a_parking_boy() {
        ParkingLot parkingLot = new ParkingLot(10);
        List<ParkingLot> parkingLots = new ArrayList<ParkingLot>();
        parkingLots.add(parkingLot);
        StandardParkingBoy standardParkingBoy = new StandardParkingBoy(parkingLots);
        Car car1 = new Car();
        Ticket ticket = standardParkingBoy.park(car1);
        Car car2 = standardParkingBoy.fetch(ticket);
        Assertions.assertEquals(car1, car2);
    }

    @Test
    void should_throw_exception_when_given_a_car_but_no_parking_position() {
        ParkingLot parkingLot = new ParkingLot(1);
        List<ParkingLot> parkingLots = new ArrayList<>();
        parkingLots.add(parkingLot);
        StandardParkingBoy standardParkingBoy = new StandardParkingBoy(parkingLots);
        Car car1 = new Car();
        standardParkingBoy.park(car1);
        Car car2 = new Car();
        ParkingException parkingException = Assertions.assertThrows(ParkingException.class, () -> standardParkingBoy.park(car2));
        Assertions.assertEquals("No available position.", parkingException.getMessage());
    }

    @Test
    void should_throw_exception_when_given_a_error_parking_ticket() {
        ParkingLot parkingLot = new ParkingLot(10);
        List<ParkingLot> parkingLots = new ArrayList<>();
        parkingLots.add(parkingLot);
        StandardParkingBoy standardParkingBoy = new StandardParkingBoy(parkingLots);
        Ticket ticket = new Ticket();
        ParkingException parkingException = Assertions.assertThrows(ParkingException.class, () -> standardParkingBoy.fetch(ticket));
        Assertions.assertEquals("Unrecognized parking ticket.", parkingException.getMessage());
    }

    @Test
    void should_throw_exception_when_given_a_used_parking_ticket() {
        ParkingLot parkingLot = new ParkingLot(10);
        List<ParkingLot> parkingLots = new ArrayList<>();
        parkingLots.add(parkingLot);
        StandardParkingBoy standardParkingBoy = new StandardParkingBoy(parkingLots);
        Car car1 = new Car();
        Ticket ticket = standardParkingBoy.park(car1);
        Car car2 = standardParkingBoy.fetch(ticket);
        Assertions.assertEquals(car1, car2);
        ParkingException parkingException = Assertions.assertThrows(ParkingException.class, () -> standardParkingBoy.fetch(ticket));
        Assertions.assertEquals("Unrecognized parking ticket.", parkingException.getMessage());
    }

    @Test
        void should_return_the_first_parking_lot_ticket_when_given_a_car_and_two_available_parking_lot() {
            Car car = new Car();
            ParkingLot parkingLot1 = new ParkingLot(10);
            ParkingLot parkingLot2 = new ParkingLot(10);
            List<ParkingLot> parkingLots = new ArrayList<>();
            parkingLots.add(parkingLot1);
            parkingLots.add(parkingLot2);
            StandardParkingBoy standardParkingBoy = new StandardParkingBoy(parkingLots);
            Ticket ticket = standardParkingBoy.park(car);
            Assertions.assertEquals(parkingLot1,ticket.getParkingLot());
    }

    @Test
    void should_return_the_second_parking_lot_ticket_when_given_a_car_and_only_seccond_parking_lot_available() {
        ParkingLot parkingLot1 = new ParkingLot(1);
        ParkingLot parkingLot2 = new ParkingLot(10);
        List<ParkingLot> parkingLots = new ArrayList<>();
        parkingLots.add(parkingLot1);
        parkingLots.add(parkingLot2);
        Car car1 = new Car();
        Car car2 = new Car();
        StandardParkingBoy standardParkingBoy = new StandardParkingBoy(parkingLots);
        standardParkingBoy.park(car1);
        Ticket ticket = standardParkingBoy.park(car2);
        Assertions.assertEquals(parkingLot2,ticket.getParkingLot());
    }

    @Test
    void should_throw_exception_when_given_a_car_but_both_parking_lots_without_position() {
        ParkingLot parkingLot1 = new ParkingLot(1);
        ParkingLot parkingLot2 = new ParkingLot(1);
        List<ParkingLot> parkingLots = new ArrayList<>();
        parkingLots.add(parkingLot1);
        parkingLots.add(parkingLot2);
        StandardParkingBoy standardParkingBoy = new StandardParkingBoy(parkingLots);
        Car car1 = new Car();
        Car car2 = new Car();
        standardParkingBoy.park(car1);
        standardParkingBoy.park(car2);
        Car car3 = new Car();
        ParkingException parkingException = Assertions.assertThrows(ParkingException.class, () -> standardParkingBoy.park(car3));
        Assertions.assertEquals("No available position.", parkingException.getMessage());
    }

    @Test
    void should_return_two_right_cars_when_given_two_car_in_two_parking_lots() {
        ParkingLot parkingLot1 = new ParkingLot(1);
        ParkingLot parkingLot2 = new ParkingLot(10);
        List<ParkingLot> parkingLots = new ArrayList<>();
        parkingLots.add(parkingLot1);
        parkingLots.add(parkingLot2);
        StandardParkingBoy standardParkingBoy = new StandardParkingBoy(parkingLots);
        Car car1 = new Car();
        Car car2 = new Car();
        Ticket ticket1 = standardParkingBoy.park(car1);
        Ticket ticket2 = standardParkingBoy.park(car2);
        Car car3 = standardParkingBoy.fetch(ticket1);
        Car car4 = standardParkingBoy.fetch(ticket2);
        Assertions.assertEquals(car1,car3);
        Assertions.assertEquals(car2,car4);
    }
}
