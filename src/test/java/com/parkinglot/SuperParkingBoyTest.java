package com.parkinglot;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class SuperParkingBoyTest {
    @Test
    void should_return_a_parking_ticket_when_given_a_car_and_a_available_parking_lot() {
        Car car = new Car();
        ParkingLot parkingLot = new ParkingLot(10);
        List<ParkingLot> parkingLots = new ArrayList<ParkingLot>();
        parkingLots.add(parkingLot);
        SuperParkingBoy superParkingBoy = new SuperParkingBoy(parkingLots);
        Ticket ticket = superParkingBoy.park(car);
        Assertions.assertNotNull(ticket);
    }

    @Test
    void should_return_a_high_available_rate_parking_lot_ticket_when_given_a_car_and_two_parking_lots() {
        Car car = new Car();
        ParkingLot parkingLot1 = new ParkingLot(3);
        ParkingLot parkingLot2 = new ParkingLot(6);
        List<ParkingLot> parkingLots = new ArrayList<>();
        parkingLots.add(parkingLot1);
        parkingLots.add(parkingLot2);
        SuperParkingBoy superParkingBoy = new SuperParkingBoy(parkingLots);
        for (int i = 0; i < 5; i++) {
            superParkingBoy.park(new Car());
        }
        Ticket ticket = superParkingBoy.park(car);
        Assertions.assertNotNull(ticket);
        Assertions.assertEquals(parkingLot2, ticket.getParkingLot());
    }

    @Test
    void should_throw_exception_when_given_a_car_but_both_parking_lots_without_position() {
        ParkingLot parkingLot1 = new ParkingLot(1);
        ParkingLot parkingLot2 = new ParkingLot(1);
        List<ParkingLot> parkingLots = new ArrayList<>();
        parkingLots.add(parkingLot1);
        parkingLots.add(parkingLot2);
        SuperParkingBoy superParkingBoy = new SuperParkingBoy(parkingLots);
        Car car1 = new Car();
        Car car2 = new Car();
        superParkingBoy.park(car1);
        superParkingBoy.park(car2);
        ParkingException parkingException = Assertions.assertThrows(ParkingException.class, () -> superParkingBoy.park(new Car()));
        Assertions.assertEquals("No available position.", parkingException.getMessage());
    }

    @Test
    void should_return_a_corresponding_car_when_given_a_valid_parking_ticket() {
        ParkingLot parkingLot = new ParkingLot(10);
        List<ParkingLot> parkingLots = new ArrayList<ParkingLot>();
        parkingLots.add(parkingLot);
        SuperParkingBoy superParkingBoy = new SuperParkingBoy(parkingLots);
        Car car1 = new Car();
        Ticket ticket = superParkingBoy.park(car1);
        Car car2 = superParkingBoy.fetch(ticket);
        Assertions.assertEquals(car1, car2);
    }

    @Test
    void should_throw_exception_when_given_a_error_parking_ticket() {
        ParkingLot parkingLot = new ParkingLot(10);
        List<ParkingLot> parkingLots = new ArrayList<>();
        parkingLots.add(parkingLot);
        SuperParkingBoy superParkingBoy = new SuperParkingBoy(parkingLots);
        ParkingException parkingException = Assertions.assertThrows(ParkingException.class, () -> superParkingBoy.fetch(new Ticket()));
        Assertions.assertEquals("Unrecognized parking ticket.", parkingException.getMessage());
    }

    @Test
    void should_throw_exception_when_given_a_used_parking_ticket() {
        ParkingLot parkingLot = new ParkingLot(10);
        List<ParkingLot> parkingLots = new ArrayList<>();
        parkingLots.add(parkingLot);
        SuperParkingBoy superParkingBoy = new SuperParkingBoy(parkingLots);
        Car car1 = new Car();
        Ticket ticket = superParkingBoy.park(car1);
        Car car2 = superParkingBoy.fetch(ticket);
        Assertions.assertEquals(car1, car2);
        ParkingException parkingException = Assertions.assertThrows(ParkingException.class, () -> superParkingBoy.fetch(ticket));
        Assertions.assertEquals("Unrecognized parking ticket.", parkingException.getMessage());
    }
}
